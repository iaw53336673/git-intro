## Fitxer xungu

Aquest fitxer no és interessant. El creem per poder fer un commit que le contingui i després practicarem la màquina del temps per recuperar una versió anterior.

Una opció que no faerm servir és reset:
```
git reset
```
Aquesta ordre elimina el darrer commit i ens quedem amb el penúltim (en realitat fem que HEAD apunti al penúltim commit en comptes de l'últim que liminem). Aquesta opció és molt poc recomanable quan es treballa en equip per raons òbvies .

L'opció que farem servir copia la penúltima opció després de la última:
```
git revert [6c7ff97d9e88ab88c02ff89362cc3e1f361af71a]
```
